<?php

Route::group(['middleware' => 'cors'], function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('register', 'Auth\AuthController@register');
    Route::get('/images/{path}', 'MediaController@getImage');
    Route::get('/video/{videoId}', 'MediaController@getVideo');
    Route::get('/excel', 'MediaController@excel');

    Route::namespace('Guest')->group( function () {
        Route::group(['prefix' => 'guest'], function () {
            Route::get('videos', 'GuestController@getVideos');
        });

//    Route::post('register', 'Auth\AuthController@register');
//    Route::get('/images/{path}', 'MediaController@getImage');
//    Route::get('/video/{videoId}', 'MediaController@getVideo');
//    Route::get('/excel', 'MediaController@excel');
    });
});

Route::group(['middleware' => ['auth.jwt', 'cors']], function () {
    Route::get('logout', 'Auth\AuthController@logout');

    Route::namespace('Category')->group(function () {
        Route::group(['prefix' => 'category'], function () {
            Route::post('/', 'CategoryController@store');
            Route::get('/', 'CategoryController@getAll');
            Route::post('/{category_id}', 'CategoryController@update')->where(['category_id' => '[0-9]+']);
            Route::delete('/{category_id}', 'CategoryController@destroy')->where(['category_id' => '[0-9]+']);
            Route::patch('/delete_selected', 'CategoryController@destroySelected');
        });
    });

    Route::namespace('Video')->group(function () {
        Route::group(['prefix' => 'video'], function () {
            Route::post('/', 'VideoController@store');
            Route::post('/upload_video/{videoId}', 'VideoController@uploadVideo');
            Route::get('/', 'VideoController@getAll');
            Route::post('/{video_id}', 'VideoController@update');
            Route::delete('/{video_id}', 'VideoController@destroy');
            Route::put('/toggle_publish/{video_id}', 'VideoController@togglePublish');
//            Route::patch('/delete_selected', 'CategoryController@destroySelected');
        });
    });












    /*web routes*/
    Route::namespace('Web')->group(function () {
        Route::group(['prefix' => 'web'], function () {
            Route::namespace('Image')->group(function () {
                Route::group(['prefix' => 'image/images'], function () {
                    Route::get('/{path}', 'ImageController@getImage');
                });
            });
            Route::namespace('Video')->group(function () {
                Route::group(['prefix' => 'video'], function () {
                    Route::get('/', 'VideoController@getAll');
                });
            });
        });
    });







});