<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 7/12/2020
 * Time: 11:44 PM
 */

namespace App\Modules\Auth;


use App\Modules\Auth\Models\User;
use Illuminate\Http\Request;

class UserService
{
    public static function remember(Request $request)
    {
        if ($request->remember_me){
            $user = User::where('email', $request->email)->first();
            $token = bcrypt($request->header('User-Agent').$request->ip());
            $rememberToken = bcrypt($user->id).'//**//..//'.$token;

            $user->remember_token = $rememberToken;
            $user->update();

            return $rememberToken;
        }

        return null;
    }

    public static function checkRememberToken(Request $request)
    {
        $token = explode('//**//..//',$request->remember_token);
        $user = User::where('remeber_token', $token[1])->first();
        if ($user) {
            $id = bcrypt($user->id);
            $user = $token[0] === $id ? $user:null;
        }

        return $user;
    }
}