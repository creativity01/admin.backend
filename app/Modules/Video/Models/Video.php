<?php

namespace App\Modules\Video\Models;

use App\Modules\Category\Models\Category;
use App\Modules\Video\Services\VideoStatusServic;
use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use UsesUuid;

    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function togglePublish()
    {
        $this->published = $this->published ? 0:1;
    }

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function scopeFreeVideos($query)
    {
        return $query->where('video_payment_status_id', VideoStatusServic::FREE_PAYMENT_STATUS_ID);
    }
}
