<?php


namespace App\WebModules\Video\Repositories;


use App\Modules\Video\Models\Video;

class VideoRepository
{
    public function getAll()
    {
        $videos = Video::with(['category'])->getPublished()->get();

        return $videos;
    }
}