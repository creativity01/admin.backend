<?php


namespace App\Http\Controllers;


use App\Modules\Video\Models\Video;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{
    public function getImage($path)
    {
        return Storage::get("images/".$path);
    }

    public function getVideo($videoId)
    {
        $video = Video::whereId($videoId)->first();

        return Storage::get($video->video);
    }



    public function excel()
    {
        $csv = Storage::get("file.csv");

        return $csv;
    }

}