<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 7/2/2020
 * Time: 2:16 PM
 */

namespace App\Http\Controllers\Guest;


use App\Http\Controllers\Controller;
use App\Modules\Video\Models\Video;

class GuestController extends Controller
{
    public function getVideos()
    {
        $videos = Video::freeVideos()->published()->get();

        return compact('videos');
    }
}