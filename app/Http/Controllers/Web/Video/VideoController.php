<?php

namespace App\Http\Controllers\Web\Video;

use App\Http\Controllers\Controller;
use App\WebModules\Video\Repositories\VideoRepository;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    private $videoRepository;

    public function __construct(VideoRepository $videoRepository)
    {
        $this->videoRepository = $videoRepository;
    }

    public function getAll()
    {
        $videos = $this->videoRepository->getAll();

        return compact('videos');
    }
}
